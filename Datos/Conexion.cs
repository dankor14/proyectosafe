﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.DataAccess.Client;
using Entidades;

namespace Datos
{
    public class Conexion
    {
        OracleConnection ora = new OracleConnection("DATA SOURCE = xe ; PASSWORD = 123; USER ID = Prueba");
        public void AbrirConexion()
        {
            try
            {
                ora.Open();
                Console.WriteLine("conexion abierta");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error" + ex.Message);
            }
        }

        public void CerrarConexion()

        {
            ora.Close();
        }

        public bool conexionLogin(string r, string cl)
        {
            bool conectado = false;
            AbrirConexion();
            OracleCommand cmd = new OracleCommand("SELECT * FROM USUARIOMASTER WHERE RUT = :rut AND CLAVE = :clave", ora);
            cmd.Parameters.Add(":rut", r);
            cmd.Parameters.Add(":clave", cl);
            try
            {
                OracleDataReader lector = cmd.ExecuteReader();

                if (lector.Read())
                {
                    conectado = true;
                }
            }
            catch (Exception ex)
            {

                Console.WriteLine("error" + ex.Message);
            }

            return conectado;

        }

        public Entidades.UsuarioMaster retornarUsuario(string r)
        {
            Entidades.UsuarioMaster usuario = new UsuarioMaster();
            AbrirConexion();
            OracleCommand cmd = new OracleCommand("SELECT * FROM USUARIOMASTER WHERE RUT = :rut", ora);
            cmd.Parameters.Add(":rut", r);
            OracleDataReader lector = cmd.ExecuteReader();
            if (lector.Read())
            {
                usuario.Rut = lector["RUT"].ToString();
                usuario.Nombre = lector["NOMBRE"].ToString();
                usuario.Apellido = lector["APELLIDO"].ToString();
                usuario.TipoUsuario = int.Parse(lector["IDTIPOUSUARIO"].ToString());
            }
            CerrarConexion();

            return usuario;
        }

        public List<Entidades.Empresa> retornarEmpresas()
        {
            List<Entidades.Empresa> ListEmpresa = new List<Empresa>();
            AbrirConexion();
            OracleCommand cmd = new OracleCommand("SELECT CODIGO, NOMBRE , DIRECCION, TELEFONO FROM EMPRESACLIENTE", ora);
            OracleDataReader lector = cmd.ExecuteReader();

            while (lector.Read())
            {
                Entidades.Empresa emp = new Empresa();
                emp.Codigo = lector.GetValue(0).ToString();
                emp.Nombre = lector.GetValue(1).ToString();
                emp.Direccion = lector.GetValue(2).ToString();
                emp.Telefono = int.Parse(lector.GetValue(3).ToString());

                ListEmpresa.Add(emp);
            }
            CerrarConexion();
            return ListEmpresa;
        }


        public bool llamarProcedimientoCrearEval(string cod, string titulo, string rutTec, string rutSup, string rutIng, string observa, string codEmpresa, int tipoEval, int estadoEval)
        {
            bool agrego = false;
            string procedimiento = "crearevaluacion";
            AbrirConexion();
            OracleCommand cmd = new OracleCommand(procedimiento, ora);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.Add("newcodigo", cod);
            cmd.Parameters.Add("newTitulo", titulo);
            cmd.Parameters.Add("newObservacion", observa);
            cmd.Parameters.Add("newRutTecnico", rutTec);
            cmd.Parameters.Add("newRutSupervisor", rutSup);
            cmd.Parameters.Add("newRutIng", rutIng);
            cmd.Parameters.Add("newCodigoEmp", codEmpresa);
            cmd.Parameters.Add("newTipoEval", tipoEval);
            cmd.Parameters.Add("newEstadoEval", estadoEval);


            OracleParameter numeroSalida = new OracleParameter();
            numeroSalida = cmd.Parameters.Add("numeroSalida", OracleDbType.Int32, System.Data.ParameterDirection.Output);
            numeroSalida.Size = 25;



            try
            {
                cmd.ExecuteNonQuery();
                int numero = int.Parse(numeroSalida.Value.ToString());
                if (numero == 1)
                {
                    agrego = true;
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine("error" + ex.Message);
            }



            return agrego;
        }

        public bool llamarProcedimientoCrearEvalPer(string cod, string rutTra)
        {
            bool agrego = false;
            string procedimiento = "crearevaluacionpersona";
            AbrirConexion();
            OracleCommand cmd = new OracleCommand(procedimiento, ora);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.Add("newcodigo", cod);
            cmd.Parameters.Add("newRut", rutTra);

            OracleParameter numeroSalida = new OracleParameter();
            numeroSalida = cmd.Parameters.Add("numeroSalida", OracleDbType.Int32, System.Data.ParameterDirection.Output);
            numeroSalida.Size = 25;


            try
            {

                cmd.ExecuteNonQuery();
                int numero = int.Parse(numeroSalida.Value.ToString());
                if (numero == 1)
                {
                    agrego = true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("error" + ex.Message);
            }



            return agrego;
        }

    }
}
