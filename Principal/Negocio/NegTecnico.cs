﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Datos; 

namespace Negocio
{
   public class NegTecnico
    {
        Conexion con = new Conexion();
        public List<Empresa> ReturnEmpresas()
        {
            return con.retornarEmpresas();
        }

        public bool agregarEvaluacionPersona(string cod, string titulo, string observa, string rutTec, string rutSup, string rutIng, string rutTraba, string codEmpre, int tipoEval, int estadoEval )
        {
            bool agregado = false;
            rutIng = "No asignado";
            rutSup = "No asignado";
            try
            {
                if (con.llamarProcedimientoCrearEval(cod, titulo, rutTec, rutSup, rutIng, observa, codEmpre, tipoEval, estadoEval) && con.llamarProcedimientoCrearEvalPer(cod, rutTraba))
                {
                    agregado = true;
                }

            }
            catch (Exception ex )
            {

                Console.WriteLine("" + ex.Message);
            }



            return agregado; 
        }
    }
}
