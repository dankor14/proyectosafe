﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Empresa
    {
        string codigo;
        string nombre;
        string direccion;
        int telefono; 


        public Empresa()
        {
            Codigo = string.Empty;
            Nombre = string.Empty;
            Direccion = string.Empty;
            Telefono = 0; 
        }

        public Empresa(string codigo, string nombre, string direccion, int telefono)
        {
            this.Codigo = codigo;
            this.Nombre = nombre;
            this.Direccion = direccion;
            this.Telefono = telefono;
        }

        public string Codigo { get => codigo; set => codigo = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public string Direccion { get => direccion; set => direccion = value; }
        public int Telefono { get => telefono; set => telefono = value; }
    }
}
