﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class UsuarioMaster
    {
        string rut;
        string nombre;
        string apellido;
        int tipoUsuario;

        public UsuarioMaster()
        {
            Rut = string.Empty;
            Nombre = string.Empty;
            Apellido = string.Empty;
            TipoUsuario = 0; 
        }

        public string Rut { get => rut; set => rut = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public string Apellido { get => apellido; set => apellido = value; }
        public int TipoUsuario { get => tipoUsuario; set => tipoUsuario = value; }
    }
}
