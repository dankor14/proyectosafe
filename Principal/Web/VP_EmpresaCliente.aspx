﻿Dentro de vp_empresaCliente.aspx pegar lo siguiente:
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VP_EmpresaCliente.aspx.cs" Inherits="Web.VP_EmpresaCliente" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>    
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <script src="Scripts/jquery-3.0.0.min.js"></script>
    <script src="Scripts/bootstrap.min.js"></script>
    <script src="Scripts/popper.min.js"></script>
    <title>Vista empresa</title>
</head>
<body>
     <div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
      <h5 class="my-0 mr-md-auto font-weight-normal">Bienvenido(a) <asp:Label ID="lblNombreUsario" runat="server"></asp:Label></h5>
      <nav class="my-2 my-md-0 mr-md-3">
        <a class="btn btn-outline-primary" href="Login.aspx" >Cerrar Sesión</a>
      </nav>
    </div>

    <main role="main" class="container">
      <div class="jumbotron">
        <h1>Bienvenido a Safe</h1>
        <p class="lead">Usted dispone de las siguientes opciones para navegar por nuestra página</p>
          <div class="container">
      <div class="card-deck mb-3 text-center">
       <div class="card mb-4 shadow-sm">
          <div class="card-header">
            <h4 class="my-0 font-weight-normal">Evaluaciones en Terreno</h4>
          </div>
          <div class="card-body">
            <ul class="list-unstyled mt-3 mb-4">
              <li>Ver evaluaciones realizas.</li>
              <li>La seguridad de su empresa.</li>
              <li>Riesgos evaluados.</li>
            </ul>
            <button type="button" class="btn btn-lg btn-block btn-primary">Ver las evaluaciones</button>
          </div>
        </div>
        <div class="card mb-4 shadow-sm">
          <div class="card-header">
            <h4 class="my-0 font-weight-normal">Capacitaciones programadas</h4>
          </div>
          <div class="card-body">
            <ul class="list-unstyled mt-3 mb-4">
              <li>Listas de asistentes.</li>
              <li>Capacitaciones para sus trabajadores.</li>
              <li>Capacitaciones realizadas.</li>
            </ul>
            <button type="button" class="btn btn-lg btn-block btn-primary">Ver Capacitaciones</button>
          </div>
        </div>
        <div class="card mb-4 shadow-sm">
          <div class="card-header">
            <h4 class="my-0 font-weight-normal">Visitas medicas programadas.</h4>
          </div>
          <div class="card-body">
            <ul class="list-unstyled mt-3 mb-4">
              <li>Visitas programadas.</li>
              <li>Asistencia a su empresa.</li>
              <li>Examenes para sus trabajadores.</li>
            </ul>
            <button type="button" class="btn btn-lg btn-block btn-primary">Ver visitas programadas</button>
          </div>
        </div>
      </div>
      </div>
          </div>
    </main>
</body>
</html>
