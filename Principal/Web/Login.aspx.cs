﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oracle.DataAccess.Client;


namespace Web
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnIngresar_Click(object sender, EventArgs e)
        {
            if (!(string.IsNullOrEmpty(txtUsername.Text) || string.IsNullOrEmpty(txtPassword.Text)))
            { 
            Negocio.NegLogin negL = new Negocio.NegLogin();
            if(negL.AlmacenarRutUsuario(txtUsername.Text, txtPassword.Text))
            {
                Session["USER"] = negL.retornoUsuario(txtUsername.Text);
                 if (negL.retornoUsuario(txtUsername.Text).TipoUsuario == 1)
                {
                    Response.Redirect("SupervisorWeb.aspx");
                }
                if (negL.retornoUsuario(txtUsername.Text).TipoUsuario == 6)
                {
                    Response.Redirect("TecnicoWeb.aspx");
                }

            }
                else
                {
                    Response.Write("<script language=javascript>alert('Rut o clave incorrecto');</script>");
                }
            }
            else
            {
                Response.Write("<script language=javascript>alert('No pueden estar los campos vacios');</script>");
            }

        }
    }
}