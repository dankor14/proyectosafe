﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Negocio;
using Entidades;

namespace Web
{
    public partial class AgregarEvaluacionTec : System.Web.UI.Page
    {
        NegTecnico negT = new NegTecnico();
        

        protected void Page_Load(object sender, EventArgs e)
        {
            ddlEmpresas.DataSource = negT.ReturnEmpresas();
            ddlEmpresas.DataValueField = "Codigo";
            ddlEmpresas.DataTextField = "Nombre";
            ddlEmpresas.DataBind();
        }

        protected void ddlTipoEval_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlTipoEval.SelectedIndex == 0)
            {
                txtRutTrabajadador.Enabled = false;
                txtNombreInstalacion.Enabled = false;
                btnCargarImagen.Enabled = false;

            }
            if (ddlTipoEval.SelectedIndex == 1)
            {
                txtNombreInstalacion.Enabled = true;
                btnCargarImagen.Enabled = true;
                txtRutTrabajadador.Enabled = false;

            }
            if (ddlTipoEval.SelectedIndex == 2)
            {
                txtRutTrabajadador.Enabled = true;
                txtNombreInstalacion.Enabled = false;
                btnCargarImagen.Enabled = false;
            }

        }

        protected void btnAgregarEvaluacion_Click1(object sender, EventArgs e)
        {
            UsuarioMaster user = (UsuarioMaster)Session["USER"];
            string cod = txtCodigoEval.Text;
            string titu = txtTituloEvaluacion.Text;
            string obse = txtObservacion.Text;
            string rutTec = user.Rut;
            string codEmpresa = ddlEmpresas.SelectedValue;
            string rutIng = string.Empty;
            string rutSup = string.Empty;
            int estadoEval = 1;
            string rutTraba = txtRutTrabajadador.Text;
            int tipoEval = 0;
            if (ddlTipoEval.SelectedIndex != 0)
            {
                if (ddlTipoEval.SelectedIndex == 1)
                {
                    tipoEval = 1;
                }
                else
                {
                    tipoEval = 2;
                }
            }
            else
            {
                Response.Write("<script language=javascript>alert('Debe agregar tipo evaluacion');</script>");
            }

            if (negT.agregarEvaluacionPersona(cod, titu, obse, rutTec, rutSup, rutIng, rutTraba, codEmpresa, tipoEval, estadoEval))
            {
                Response.Write("<script language=javascript>alert('Evaluacion Agrgada');</script>");
            }
            else
            {
                Response.Write("<script language=javascript>alert('No se pudo agregar');</script>");
            }
        }

    }
}