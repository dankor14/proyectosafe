﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Web.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>    
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <script src="Scripts/jquery-3.0.0.min.js"></script>
    <script src="Scripts/bootstrap.min.js"></script>
    <script src="Scripts/popper.min.js"></script>
    <title>Login Safe</title>
</head>
<body>
     <div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
      <h5 class="my-0 mr-md-auto font-weight-normal">Empresa SAFE</h5>
      <nav class="my-2 my-md-0 mr-md-3">
        <a class="p-2 text-dark" href="AcercaDeSafe.aspx">Acerca de...</a>
        <a class="p-2 text-dark" href="ContactoSAFE.aspx">Contacto</a>
      </nav>
    </div>
    <form id="form1" runat="server">
        <div class="form-group" align="center">
            <img alt="" src="/imagenes/logo.png" />
        <p class="text-dark">Rut Usuario: </p><asp:TextBox class="form-control" placeholder="Ingrese Rut" MaxLength="15" ID="txtUsername" Width="300px" runat="server"></asp:TextBox>
        <p>
            <p class="text-dark">Contraseña: </p><asp:TextBox class="form-control" placeholder="Ingrese Password" MaxLength="15" ID="txtPassword" Width="300px" runat="server" TextMode="Password"></asp:TextBox>
            <p></p>
            <asp:Button ID="btnIngresar" class="btn btn-dark" runat="server" OnClick="btnIngresar_Click" Text="ingresar" />
        </p>
            </div>
    </form>
</body>
</html>
