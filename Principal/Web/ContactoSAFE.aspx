﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Web.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>    
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <script src="Scripts/jquery-3.0.0.min.js"></script>
    <script src="Scripts/bootstrap.min.js"></script>
    <script src="Scripts/popper.min.js"></script>
    <title>Contacto Safe</title>
     <script type="text/javascript">
     
        function alerta(numero) {
            alert('Se ha presionado el boton: ' + numero);
        }
     
    </script>
</head>
<body  class="bg-light">
     <div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
      <h5 class="my-0 mr-md-auto font-weight-normal">Empresa SAFE</h5>
      <nav class="my-2 my-md-0 mr-md-3">
        <a class="p-2 text-dark" href="AcercaDeSafe.aspx">Acerca de...</a>
        <a class="btn btn-outline-primary" href="Login.aspx">Iniciar sesión</a>
      </nav>
    </div>
    <div class="container">
      <div class="py-5 text-center">
        <img class="d-block mx-auto mb-4" src="/imagenes/logoContacto.png"/>
        <h2>Gestionamos tú seguridad</h2>
        <p class="lead">Para contratar cualquiera de nuestros servicios no dudes en contactarnos...</p>
      </div>
        <div class="col-md-8 order-md-1">
          <h4 class="mb-3">Ingrese sus datos</h4>
          <form class="needs-validation">
            <div class="row">
              <div class="col-md-6 mb-3">
                <label for="firstName">Nombre empresa</label>
                <input type="text" class="form-control" id="firstName" maxlength="15" placeholder="" value="" required/>
                <div class="invalid-feedback">
                  El nombre debe ser ingresado.
                </div>
              </div>
              <div class="col-md-6 mb-3">
                <label for="lastName">Dirección</label>
                <input type="text" class="form-control" id="lastName" maxlength="15"  placeholder="" value="" required/>
                <div class="invalid-feedback">
                 La dirección debe ser ingresada.
                </div>
              </div>
                 <div class="col-md-6 mb-3">
                <label for="lastName">Email</label>
                <input type="email" class="form-control" id="email" placeholder="tuemail@example.com"/>
                <div class="invalid-feedback">
                  Usted debe ingresar un correo.
                </div>
              </div>
                 <div class="col-md-6 mb-3">
                <label for="lastName">Asunto</label>
                <textarea rows="3" class="form-control" id="txtArea"></textarea>
              </div>
            </div>
               <hr class="mb-4">
            <button class="btn btn-primary btn-lg btn-block" id="btnEmail" type="submit" onclick="alert('Se ha enviado un email con sus datos');" >Enviar Email</button>
              </form>
        </div>
        </div>
</body>
</html>

