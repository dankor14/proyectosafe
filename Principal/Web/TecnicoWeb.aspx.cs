﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Entidades;

namespace Web
{
    public partial class TecnicoWeb : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Entidades.UsuarioMaster user = (UsuarioMaster)Session["USER"];
            lblNomusuario.Text = user.Nombre;
        }
    }
}