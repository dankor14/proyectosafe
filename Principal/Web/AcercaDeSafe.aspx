﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Web.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>    
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <script src="Scripts/jquery-3.0.0.min.js"></script>
    <script src="Scripts/bootstrap.min.js"></script>
    <script src="Scripts/popper.min.js"></script>
    <title>Acerca de Safe</title>
</head>
<body  class="bg-light">
         <div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
      <h5 class="my-0 mr-md-auto font-weight-normal">Empresa SAFE</h5>
      <nav class="my-2 my-md-0 mr-md-3">
        <a class="btn btn-outline-primary" href="Login.aspx">Iniciar sesión</a>
        <a class="p-2 text-dark" href="ContactoSAFE.aspx">Contacto</a>
      </nav>
    </div>
     <div class="container">
        <!-- Example row of columns -->
        <div class="row">
          <div class="col-md-4">
            <h2>Empresas y sus instalaciones</h2>
            <p>SAFE ofrece servicios anuales para la evaluacion de las instalaciones de su empresa, evaluando riesgos y proponiendo mejoras. </p>
            <p><a class="btn btn-secondary" href="ContactoSAFE.aspx" role="button">Contactanos &raquo;</a></p>
              <img src="imagenes/instalacion.jpg" class="rounded float-left" alt="200" height="200">
          </div>
          <div class="col-md-4">
            <h2>Empresas y sus trabajadores</h2>
            <p>SAFE ofrece servicios anuales para la seguridad de los trabajadores de su empresa, otorgando además certificaciones de seguridad para sus empleados, además se hacen evaluaciones del personal para evaluar su seguridad en el trabajo.</p>
            <p><a class="btn btn-secondary" href="ContactoSAFE.aspx" role="button">Contactanos &raquo;</a></p>
              <img src="imagenes/personal.jpg" class="rounded float-left" alt="200" height="200">
          </div>
             <div class="col-md-4">
            <h2>Visitas Medicas</h2>
            <p>SAFE ofrece el servicio de agendar visitas medicas a su empresa con la finalidad de evaluar a sus trabajadores por posibles infecciones residuales y además para evaluar su condición actual.</p>
            <p><a class="btn btn-secondary" href="ContactoSAFE.aspx" role="button">Contactanos &raquo;</a></p>
                 <img src="imagenes/medico.jpg" class="rounded float-left" alt="200" height="200">
          </div>
        </div>
        </div>

</body>
</html>

