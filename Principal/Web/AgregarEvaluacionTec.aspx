﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AgregarEvaluacionTec.aspx.cs" Inherits="Web.AgregarEvaluacionTec" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title></title>
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
</head>
<body>
       <div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
      <h5 class="my-0 mr-md-auto font-weight-normal">Empresa SAFE</h5>
      <nav class="my-2 my-md-0 mr-md-3">
        <a class="btn btn-outline-primary" href="Login.aspx">Cerrar sesion</a>
      </nav>
    </div>
    <form id="form1" runat="server">
        <div class=" container , row">
            
            <div class="col-3">

            </div>
            <div class =" col-6 form-group">
                <h1>Formulario evaluacion</h1>
                <asp:Label ID="Label4" runat="server" Text="Empresa cliente*"></asp:Label> 
                <asp:DropDownList ID="ddlEmpresas" runat="server" CssClass="form-control" AutoPostBack="True"  ></asp:DropDownList> 
                <asp:Label ID="Label5" runat="server" Text="TipoEvaluacion*"></asp:Label> 
                <asp:DropDownList ID="ddlTipoEval" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlTipoEval_SelectedIndexChanged" AutoPostBack="True" >
                    <asp:ListItem>Seleccione un tipo</asp:ListItem>
                    <asp:ListItem>Instalacion</asp:ListItem>
                    <asp:ListItem>Persona</asp:ListItem>
                </asp:DropDownList> 
                <asp:Label ID="Label1" runat="server" Text="Codigo*" ></asp:Label>
                <asp:TextBox ID="txtCodigoEval" runat="server" CssClass ="form-control" placeholder="Ingrese codigo de evaluación" ></asp:TextBox>
                <asp:Label ID="Label2" runat="server" Text="Titulo*"></asp:Label>
                <asp:TextBox ID="txtTituloEvaluacion" runat="server" CssClass ="form-control" placeholder="Ingrese titulo de la evaluacion" ></asp:TextBox>
                <asp:Label ID="Label3" runat="server" Text="Observacion*"></asp:Label>
                <asp:TextBox ID="txtObservacion" runat="server" CssClass ="form-control" placeholder="Ingrese obsevacion" TextMode="MultiLine" ></asp:TextBox>
                <asp:Label ID="Label6" runat="server" Text="Rut trabajador*"></asp:Label>
                <asp:TextBox ID="txtRutTrabajadador" runat="server" CssClass ="form-control" placeholder="Ingrese rut del trabajador" Enabled="False"  ></asp:TextBox>
                <asp:Label ID="Label7" runat="server" Text="Nombre instalacion"></asp:Label>
                <asp:TextBox ID="txtNombreInstalacion" runat="server" CssClass ="form-control" placeholder="Ingrese nombre de la instalacion" Enabled="False" ></asp:TextBox> <hr />
                <asp:Button ID="btnCargarImagen" runat="server" Text="Cargar fotografia" CssClass="form-control btn-primary" Enabled="False" /> <hr />
                <asp:Button ID="btnAgregarEvaluacion" runat="server" Text="Registrar evaluacion" CssClass="form-control btn-primary" OnClick="btnAgregarEvaluacion_Click1" />



            </div>
            <div class="col-3">

            </div>

        </div>
    </form>
    <script src="http://code.jquery.com/jquery.js"></script>
    <script src="Scripts/bootstrap.min.js"></script>
</body>
</html>
